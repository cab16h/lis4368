

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Christian Burell - Information Technology Major

### Assignment 1 Requirements:


#### README.md file should include the following items:

1. Screenshot of running java Hello - version
2. Screenshot of running http://localhost:9999 
3. Link to local lis4368 web app: http://localhost:9999/lis4368/index.jsp
4. git commands w/short description;
5. Bitbucket repo links:
    * This assignment, and
    * The completed tutorial repo above (bitbucket)

#### README.md file should include the following items:

+ Screenshot of AMPPS Installations
+ Screenshot of running Java - Hello
+ Screenshot of running Tomcat
+ git commands w/short descriptions
+ Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - this command an empty Git repository. 
2. git status - displays paths that have differences between the index file and the current head commit.
3. git add . - This command updates the index using the current content found in the working tree.
4. git commit -m "description" - stores the current contents of the index in a new commit along with a log message from the user describing the changes.
5. git push -u origin master - updates remote refs using local refs, while sending objects necessary to complete the given refs.
6. git pull - Incorporates changes from a remote repository into the current branch
7. git clone - clones a repository into a newly created directory.
 

#### Assignment Screenshots:


*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/HelloWorld.jpg)

*Screenshot of Tomcat*:

![Tomcat Installation Screenshot](img/tomcatinstall.jpg)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
