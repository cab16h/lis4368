

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Christian Burell - Information Technology Major

### Project 2 Requirements:


#### README.md file should include the following items:

1. Screenshot of errors
2. Screenshot of successful
3. Link to local lis4368 p1 web app: http://localhost:9999/lis4368/p1


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

 

#### Assignment Screenshots:


#### Assignment Screenshot and Links:
Passed Validation                           |  Inserts
:------------------------------------------:|:------------------------------------------:
![FAILED VALIDATION](img/adding.png)     |![SUCCESSFUL VALIDATION](img/customer.png)

MYSQL                          |  
:------------------------------------------:|
![FAILED VALIDATION](img/update.png)     |




