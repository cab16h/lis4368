

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Christian Burell - Information Technology Major

### Assignment 3 Requirements:


#### README.md file should include the following items:

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records each table)
 3. Provide bitbucker read only access to repo (Language SQL). Must include README.md using Markdown syntax, and include links to all of the following files)   
    - docs folder: a3.mwb and a3.sql
    - img folder: a3.png (export a3.mwb file as a3.pgn)
    - README.md (must display a3.png ERD)
4. Canvas links bitbucket repo

    - screenshot of erd links to the image
 





#### Assignment Screenshot and Links:
*Screenshot A3 ERD*:
![A3 ERD](img/lis4368_a3.png "ERD based upon A3 Requirements")
*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/lis4368_a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/lis4368_a3.sql "A3 SQL Script")



