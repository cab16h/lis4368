> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advance Web Appliction

## Christian Burell

### Lis 4368 Requirements: 

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat

2. [A2 README.md](a2/README.md "My A1 README.md file")
    - Links display properly
    - Querybook screenshot

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - ERD
    - 10 records
    - screenshots of ERD
    - A3 mwb file and sql file

4. [P1 README.md](p1/README.md "My p1 README.md file")
    - succesful attempt screenshot
    - failed attemp screenshot

5. [A4 README.md](a4/README.md "My a4 README.md file")
    - succesful attempt screenshot
    - failed attemp screenshot
    - links to assignments

6. [A5 README.md](a5/README.md "My a5 README.md file")
    - succesful attempt screenshot
    - mysql screenshot
    - links to assignments
   
7. [P2 README.md](p2/README.md "My p2 README.md file")
    - succesful attempt screenshot
    - mysql screenshot
    - links to assignments
   