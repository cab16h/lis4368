

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Christian Burell - Information Technology Major

### Project 1 Requirements:


#### README.md file should include the following items:

1. Screenshot of errors
2. Screenshot of successful
3. Link to local lis4368 p1 web app: http://localhost:9999/lis4368/p1/index.jsp


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

 

#### Assignment Screenshots:


*Screenshot of running java Hello*:

![Failed attempt](img/failed.png)

*Screenshot of Tomcat*:

![Success attempt](img/success.png)



